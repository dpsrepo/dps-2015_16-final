# DPS Step 2

Poniższe repozytorium zawiera dane niezbędne do przeprowadzenia analizy drugiego etapu projektu DPS.

## Pliki wejściowe

- [dane z Kibany](data/raw/dps_08_Jul_2016_03_00_EDT.json) (dump z 8 lipca),
- dane z GA - [Spindl](data/raw/Analytics skiareal Spindl Odbiorcy ogółem 20151201-20160430.xlsx), [jasna.sk](data/raw/Analytics www.jasna.sk Odbiorcy ogółem 20151201-20160430.xlsx), [vt.sk](data/raw/Analytics www.vt.sk Odbiorcy ogółem 20151201-20160430.xlsx),
- dane sprzedażowe TMR [wszystkie](data/raw/trm-all-sales.xlsx) i [eshop](data/raw/tmr-eshop-sales.xlsx)

## Skrypty

- [skrypt](scripts/fetch_from_b24.py) pobierający dane archiwalne z Brand24 ([brand24 csv](data/b24.csv)),
- [skrypt](scripts/parse_es.py) parsujący dane wizualizowane w Kibanie ([resorts csv](data/resorts,csv), [countries csv](data/countries.csv))

## Notatniki

- [notatnik](notebooks/Hipotezy.ipynb) opisujący proces otrzymania finalnego zbioru danych,
- [notatnik](notebooks/Przygotowanie danych.ipynb) weryfikujący hipotezy

## Raport

- Finalny [raport](report/Raport_pdf.pdf) z przeprowadzonego drugiego etapu,
- [arkusz](data/dps-step2.xlsx) z otrzymanymi danymi

Do załadowania notatników zalecane jest użycie przygotowanego wcześniej kontenera Dockera.

    docker-compose up
