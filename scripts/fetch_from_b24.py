#!/usr/bin/env python3

import pandas as pd
import requests
from xml.etree import ElementTree

print('=' * 5 + ' Brand24 data fetcher ' + '=' * 5)

# Constants
API_KEY = '229a5694a84937c539873e7947ece5de'
DAY_FROM = '2015-12-01'
DAY_TO = '2016-04-30'

OUT_FILE = '../data/b24.csv'

PROJECTS = {
    'Jasná Nízke Tatry': 40503223,
    'Špindlerův Mlýn': 42509115,
    'Tatranská Lomnica': 40503720
}

df = pd.DataFrame()

for resort, sid in PROJECTS.items():
    print('\nGetting data for: {}'.format(resort))

    for day in pd.date_range(DAY_FROM, DAY_TO):
        pretty_day = day.strftime("%Y-%m-%d")
        print('\tDay: {}'.format(pretty_day))
        # api call
        url = "https://api.brand24.pl/api/results-count/?key={}&sid={}&d1={}&d2={}".format(API_KEY, sid, pretty_day, pretty_day)
        #print(url)
        response = requests.get(url)
        root = ElementTree.fromstring(response.content)

        m = pd.DataFrame({
            'day': [day],
            'resort': [resort],
            'positive': root.find('positive').text,
            'negative': root.find('negative').text,
            'neutral': root.find('neutral').text
        })

        df = df.append(m)

print("\nSaving to CSV file ...")
df.to_csv(OUT_FILE, sep='\t')
