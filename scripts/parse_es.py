#!/usr/bin/env python3

import json
import pandas as pd

print('=' * 5 + ' Elasticsearch data parser ' + '=' * 5)

countries = pd.DataFrame()
resorts = pd.DataFrame()

print('Loading data file ...')
with open('../data/raw/dps_08_Jul_2016_03_00_EDT.json') as data_file:
    data = json.load(data_file)

    for row in data:
        # work with resorts
        if (row['_type']) == 'resort':

            ski_pass_price = 0
            if 'ski_pass_price' in row['_source'].keys():
                if row['_source']['ski_pass_price'] != None:
                    ski_pass_price = row['_source']['ski_pass_price']

            r = pd.DataFrame({
                'country': [row['_source']['country']],
                'lat': [row['_source']['geo'][1]],
                'lon':[row['_source']['geo'][0]],
                'resort': [row['_source']['resort']],
                'season': [row['_source']['season']],
                'date': [row['_source']['date']],
                'ski_pass_price': [ski_pass_price],
                'active_lifts': [row['_source']['active_lifts']],
                'active_slopes': [row['_source']['active_slopes']],
                'snow_3h': [row['_source']['weather']['snow_3h']],
                'rain_3h': [row['_source']['weather']['rain_3h']],
                'wind_speed': [row['_source']['weather']['wind_speed']],
                'pressure': [row['_source']['weather']['pressure']],
                'temperature': [row['_source']['weather']['temperature']],
                'overall_conditions': [row['_source']['weather']['overall_conditions']],
                'humidity': [row['_source']['weather']['humidity']],
                'cloudiness': [row['_source']['weather']['cloudiness']]
            })
            resorts = resorts.append(r)

        # work with countries
        if (row['_type']) == 'country':
            c = pd.DataFrame({
                'country': [row['_source']['country']],
                'date': [row['_source']['date']],
                'holiday': [row['_source']['holiday']],
                'fuel_eur_price': [row['_source']['fuel_eur_price']]
            })
            countries = countries.append(c)

print('Done parsing')
print('\nGot {} countries rows'.format(len(countries)))
print(countries.head())

print('\nGot {} resort rows'.format(len(resorts)))
print(resorts.head())

print('\nSaving to CSV')
resorts.to_csv('../data/resorts.csv', sep='\t')
countries.to_csv('../data/countries.csv', sep='\t')
