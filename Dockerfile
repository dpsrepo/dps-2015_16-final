# Copyright (c) Parrot Prediction Ltd.
# Distributed under the terms of the Modified BSD License.
FROM jupyter/scipy-notebook

MAINTAINER Parrot Prediction <contact@parrotprediction.com>

# For parsing Excel files
RUN pip3 install xlrd openpyxl
